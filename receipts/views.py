from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm


# Create your views here.
@login_required
def home(request):
    all_user_receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"all_receipts": all_user_receipts}
    return render(request, "receipts/receipt_list.html", context)

@login_required
def create_receipts(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            new_receipt = form.save(False)
            new_receipt.purchaser = request.user
            new_receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form" : form
    }
    return render(request, "receipts/create_receipts.html", context)

@login_required
def category_list(request):
    my_cate_list = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category_list" : my_cate_list
    }
    return render(request, "receipts/category_list.html", context)


@login_required
def account_list(request):
    my_acc_list = Account.objects.filter(owner=request.user)
    context = {
        "account_list" : my_acc_list
    }
    return render(request, "receipts/account_list.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            new_cate = form.save(False)
            new_cate.owner = request.user
            new_cate.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form" : form
    }
    return render(request, "receipts/create_category.html", context)

@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            new_acc = form.save(False)
            new_acc.owner = request.user
            new_acc.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form" : form
    }
    return render(request, "receipts/create_account.html", context)
